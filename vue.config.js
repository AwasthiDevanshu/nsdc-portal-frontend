module.exports = {
  lintOnSave: false,

  configureWebpack: {
    devtool: 'source-map',
    //Necessary to run npm link https://webpack.js.org/configuration/resolve/#resolve-symlinks
    resolve: {
       symlinks: false
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000,
      }
    }
  },

  runtimeCompiler: true,
  
  transpileDependencies: [
    '@coreui/utils'
  ],

  devServer: {
    disableHostCheck: true
  }

  ,
  pwa: {
    name: 'Nsdc App',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'service-worker.js',
      // ...other Workbox options...
    }
  }
}
