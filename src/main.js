import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import VueQuillEditor from 'vue-quill-editor'
import 'vue-search-select/dist/VueSearchSelect.css';
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import './registerServiceWorker'

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.use(VueQuillEditor)

Vue.prototype.$log = console.log.bind(console)

new Vue({
  el: '#app',
  router,
  store,
  icons,
  comments: true, 
  template: '<App/>',
  components: {
    App
  }
})
