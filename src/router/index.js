import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const SendMail = () => import('@/views/mail/SendMail')

const EventList = () => import('@/views/event/EventList')
const AddEvent = () => import('@/views/event/AddEvent')
const AddTestPaper = () => import('@/views/test/AddTestPaper')
const AddAssessor = () => import('@/views/assessor/AddEditAssessor')
const CandidateList = () => import('@/views/event/CandidateList')
const Rescore = () => import('@/views/event/Rescore')
const ProctoringData = () => import('@/views/event/proctoring-data/ProctoringData')
const Documents = () => import('@/views/event/Documents')
const SkillList = () => import('@/views/skill/SkillList')
const SchemeList = () => import('@/views/scheme/SchemeList')
const SectorList = () => import('@/views/sector/SectorList')
const AddEditSkill = () => import('@/views/skill/AddEditSkill')
const TrainingPartnerList = () => import('@/views/trainingPartner/TrainingPartnerList')
const AssessorList = () => import('@/views/assessor/AssessorList')
const AddCutoff = () => import('@/views/cutoff/Cutoff')
// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

/*
const Dashboard = () => import('@/views/Dashboard')

const Colors = () => import('@/views/theme/Colors')
const Typography = () => import('@/views/theme/Typography')

const Charts = () => import('@/views/charts/Charts')
const Widgets = () => import('@/views/widgets/Widgets')

// Views - Components
const Cards = () => import('@/views/base/Cards')
const Forms = () => import('@/views/base/Forms')
const Switches = () => import('@/views/base/Switches')
const Tables = () => import('@/views/base/Tables')
const Tabs = () => import('@/views/base/Tabs')
const Breadcrumbs = () => import('@/views/base/Breadcrumbs')
const Carousels = () => import('@/views/base/Carousels')
const Collapses = () => import('@/views/base/Collapses')
const Jumbotrons = () => import('@/views/base/Jumbotrons')
const ListGroups = () => import('@/views/base/ListGroups')
const Navs = () => import('@/views/base/Navs')
const Navbars = () => import('@/views/base/Navbars')
const Paginations = () => import('@/views/base/Paginations')
const Popovers = () => import('@/views/base/Popovers')
const ProgressBars = () => import('@/views/base/ProgressBars')
const Tooltips = () => import('@/views/base/Tooltips')

// Views - Buttons
const StandardButtons = () => import('@/views/buttons/StandardButtons')
const ButtonGroups = () => import('@/views/buttons/ButtonGroups')
const Dropdowns = () => import('@/views/buttons/Dropdowns')
const BrandButtons = () => import('@/views/buttons/BrandButtons')

// Views - Icons
const CoreUIIcons = () => import('@/views/icons/CoreUIIcons')
const Brands = () => import('@/views/icons/Brands')
const Flags = () => import('@/views/icons/Flags')

// Views - Notifications
const Alerts = () => import('@/views/notifications/Alerts')
const Badges = () => import('@/views/notifications/Badges')
const Modals = () => import('@/views/notifications/Modals')

// Users
const Users = () => import('@/views/users/Users')
const User = () => import('@/views/users/User')
*/
Vue.use(Router)

export default new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes() {
  return [
    {
      path: '/',
      redirect: '/pages/login',
      name: '',
      component: TheContainer,
      children: [
        {
          path: 'addEvent',
          name: 'Event Add',
          component: AddEvent
        },
        {
          path: 'addEvent/:eventId',
          name: 'Edit Event',
          component: AddEvent
        },
        {
          path: 'addTestPaper',
          name: 'Add Test Paper',
          component: AddTestPaper
        },
        {
          path: 'mail/sendMail',
          name: 'Send Mail',
          component: SendMail
        },
        {
          path: 'eventList',
          name: 'EventList',
          component: EventList
        },
        {
          path: 'candidates/:eventId',
          name: 'CandidateList',
          component: CandidateList
        },
        {
          path: 'candidates/:eventId/rescore',
          name: 'CandidateList',
          component: Rescore
        },
        {
          path: 'proctoringData/',
          name: 'ProctoringData',
          component: ProctoringData,
        },
        {
          path: 'documents/:eventId',
          name: 'Documents',
          component: Documents
        },
        {
          path: 'assessor/:assessorId',
          name: 'Edit Assessor',
          component: AddAssessor
        },
        {
          path: 'assessor',
          name: 'Add Assessor',
          component: AddAssessor
        },
        {
          path: 'assessorList',
          name: "Assessor List",
          component: AssessorList
        },
        {
          path: 'extra/skills',
          name: 'skillList',
          component: SkillList
        },
        {
          path: 'extra/schemes',
          name: 'schemeList',
          component: SchemeList
        },
        {
          path: 'extra/tpList',
          name: "Training Partner List",
          component: TrainingPartnerList
        },
        {
          path: 'extra/sectors',
          name: 'sectorList',
          component: SectorList
        },
        {
          path: 'extra/createCutoff',
          name: 'AddCutoff',
          component: AddCutoff
        },
        {
          path: 'extra/AddEditSkill',
          name: 'Add Skill',
          component: AddEditSkill
        },
        {
          path: '/pages',
          redirect: '/pages/404',
          name: 'Pages',
          component: {
            render(c) { return c('router-view') }
          },
          children: [
            {
              path: '404',
              name: 'Page404',
              component: Page404
            },
            {
              path: '500',
              name: 'Page500',
              component: Page500
            },

            {
              path: 'register',
              name: 'Register',
              component: Register
            }
          ]
        },

      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render(c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        }
      ]
    }
  ]
}

