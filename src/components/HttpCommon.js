import "regenerator-runtime/runtime";
import axios from 'axios'

export const HTTP = axios.create({
  baseURL: `https://backend.conicskill.com/`,
  crossDomain: true,
  headers: {
  }
})
export const ajaxCallMixin = {
  response: {},
  methods: {
    ajaxCall: async function (url, data, callBack = null, extraFields = []) {
      var form = new FormData()
      var params = JSON.stringify(data)
      form.append('body', params)
      for (let [key, value] of Object.entries(extraFields)) {
        form.append(key, value);
      }
      HTTP.post(url, form).then(res => {
        // do good things
        if (callBack != null) {
          return callBack(res.data)
        }
      }).catch(err => {
        if (err.response) {
          var res = err.response;
          if (callBack != null) {
            return callBack(res.data)
          }
        } 
        console.log(err);
      })
    },
    fileDownload: async function(url,data){
      var form = new FormData()
      var params = JSON.stringify(data)
      form.append('body',params)
      this.response = await HTTP.post(url,form,{responseType:"blob"})
      let blob = new Blob([this.response.data] ,{type: this.response.headers['content-type']}),
      downloadUrl = window.URL.createObjectURL(blob),
      filename = "",
      disposition = this.response.headers["content-disposition"];
      if (disposition && disposition.indexOf("attachment") !== -1) {
          let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/,
              matches = filenameRegex.exec(disposition);
  
          if (matches != null && matches[1]) {
              filename = matches[1].replace(/['"]/g, "");
          }
      }
      let a = document.createElement("a");
      if (typeof a.download === "undefined") {
          window.location.href = downloadUrl;
      } 
      else {
          a.href = downloadUrl;
          a.download = filename;
          document.body.appendChild(a);
          a.click();
      }
    }
  },
  
}
