export default [
  {
    _name: "CSidebarNav",
    _children: [
      {
        _name: "CSidebarNavDropdown",
        name: "Events",
        icon: "cil-calendar",
        items: [
          {
            _name: "CSidebarNavItem",
            name: "Event List",
            to: "/eventList",
            icon: "cil-calendar",
            /*badge: {
            color: 'primary',
            text: 'NEW'
          }*/
          },
          {
            _name: "CSidebarNavItem",
            name: "Add Events",
            to: "/addEvent",
            icon: "cil-playlist-add",
            /*badge: {
            color: 'primary',
            text: 'NEW'
          }*/
          },
        ],
      },
      {
        _name: "CSidebarNavDropdown",
        name: "Assessors",
        icon: "cil-user",
        items: [
          {
            name: "Add Assessor",
            to: "/assessor",
          },
          {
            name: "Assessor List",
            to: "/assessorList",
          },
        ],
      },
      {
        _name: "CSidebarNavItem",
        name: "Add Test Paper",
        to: "/addTestPaper",
        icon: "cil-file",
        /*badge: {
          color: 'primary',
          text: 'NEW'
        }*/
      },
      {
        _name: "CSidebarNavItem",
        name: "Send Mail",
        to: "/mail/sendMail",
        icon: "cil-paper-plane",
        /*badge: {
          color: 'primary',
          text: 'NEW'
        }*/
      },
      {
        _name: "CSidebarNavDropdown",
        name: "Extras",
        route: "/extras",
        icon: "cil-puzzle",
        items: [
          /*{
            name: 'Add Skill',
            to: '/extra/AddEditSkill'
          },*/
          {
            name: "Skill List",
            to: "/extra/skills",
          },
          {
            name: "Sector List",
            to: "/extra/sectors",
          },
          {
            name: "Scheme List",
            to: "/extra/schemes",
          },
          {
            name: "Training Partner",
            to: "/extra/tpList",
          },
          {
            name: "Create Cutoff",
            badge: {
          color: 'primary',
          text: 'NEW'
        },
            to: "/extra/createCutoff",
          },
        ],
      },
    ],
  },
];
